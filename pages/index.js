import React, { useEffect, useState } from "react";
import styles from "../styles/Home.module.css";
import { withAuthenticator } from "@aws-amplify/ui-react";
import { API, Auth, withSSRContext, graphqlOperation } from "aws-amplify";
import { listMessages } from "../graphql/queries";
import { createMessage } from "../graphql/mutations";
import Message from "../components/message";
import { onCreateMessage } from "../graphql/subscriptions";

function Home({ messages }) {
  const [stateMessages, setStateMessages] = useState([...messages]);
  const [messageText, setMessageText] = useState("");
  const [user, setUser] = useState(null);

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const amplifyUser = await Auth.currentAuthenticatedUser();
        setUser(amplifyUser);
      } catch (err) {
        setUser(null);
      }
    };

    fetchUser();

    // Subscribe to creation of message
    const subscription = API.graphql(
      graphqlOperation(onCreateMessage)
    ).subscribe({
      next: ({ provider, value }) => {
        setStateMessages((stateMessages) => [
          ...stateMessages,
          value.data.onCreateMessage,
        ]);
      },
      error: (error) => console.warn(error),
    });
  }, []);

  const handleSubmit = async (event) => {
    // Prevent the page from reloading
    event.preventDefault();
    console.log(event.target.value);

    // clear the textbox
    setMessageText("");

    const input = {
      // id is auto populated by AWS Amplify
      message: messageText, // the message content the user submitted (from state)
      owner: user.username, // this is the username of the current user
    };

    // Try make the mutation to graphql API
    try {
      await API.graphql({
        authMode: "AMAZON_COGNITO_USER_POOLS",
        query: createMessage,
        variables: {
          input: input,
        },
      });
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <div className={styles.background}>
    <div className={styles.container}>
      <h1 className={styles.title}> AWS Amplify Live Chat</h1>
      <div className={styles.chatbox}>
        {stateMessages
          // sort messages oldest to newest client-side
          .sort((a, b) => b.createdAt.localeCompare(a.createdAt))
          .map((message) => (
            // map each message into the message component with message as props
            <Message
              message={message}
              user={user}
              isMe={user?.username === message.owner}
              key={message.id}
            />
          ))}
      </div>
      <div className={styles.formContainer}>
        <form onSubmit={handleSubmit} className={styles.formBase}>
          <input
            type="text"
            id="message"
            name="message"
            autoFocus
            required
            value={messageText}
            onChange={(e) => setMessageText(e.target.value)}
            placeholder="💬 Send a message to the world 🌎"
            className={styles.textBox}
          />
          <button style={{ marginLeft: "8px" }}>Send</button>
        </form>
      </div>
    </div>
  </div>
  )
}

export default withAuthenticator(Home);

export async function getServerSideProps({ req }) {
  const SSR = withSSRContext({ req });

  try {
    const user = await SSR.Auth.currentAuthenticatedUser();

    const response = await SSR.API.graphql({
      query: listMessages,
      // use authMode: AMAZON_COGNITO_USER_POOLS to make a request on the current user's behalf
      authMode: "AMAZON_COGNITO_USER_POOLS",
    });

    // return all the messages from the dynamoDB
    return {
      props: {
        messages: response.data.listMessages.items,
      },
    };
  } catch (error) {
    return {
      props: {
        messages: [],
      },
    };
  }
}